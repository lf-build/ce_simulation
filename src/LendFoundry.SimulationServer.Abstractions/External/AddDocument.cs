﻿using System.Collections.Generic;

namespace LendFoundry.SimulationServer
{
    public class AddDocument
    {
        public Dictionary<string, string> SearchParameters { get; set; }
        public int MinimumMatchRequiredForSearch { get; set; }
        public bool IsXml { get; set; }
        public string Document { get; set; }
    }
}
