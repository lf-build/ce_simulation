﻿using System.Xml.Serialization;

namespace LendFoundry.SimulationServer
{
    [XmlRoot(ElementName = "ExecuteXMLString", Namespace = "http://tempuri.org/")]
    public partial class ExecuteXMLString
    {
        [XmlElement(ElementName = "request", Namespace = "http://tempuri.org/")]
        public string Request { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public partial class Body
    {
        [XmlElement(ElementName = "ExecuteXMLString", Namespace = "http://tempuri.org/")]
        public ExecuteXMLString ExecuteXMLString { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public partial class  Envelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }
        [XmlAttribute(AttributeName = "s", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string S { get; set; }
    }

}
