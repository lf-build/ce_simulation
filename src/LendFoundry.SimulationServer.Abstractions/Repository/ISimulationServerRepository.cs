﻿

using LendFoundry.Foundation.Persistence;

namespace LendFoundry.SimulationServer
{
    public interface ISimulationServerRepository : IRepository<ISimulationServerDocuments>
    {

    }
}
