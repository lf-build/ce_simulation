﻿
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.SimulationServer
{
    public interface ISimulationServerDocuments :IAggregate
    {
        string ModuleName { get; set; }
        Dictionary<string, string> SearchParameters { get; set; }
        int MinimumMatchRequiredForSearch { get; set; }
        bool IsXml { get; set; }
        bool IsXmlOutPut { get; set; }
        string Document { get; set; }
    }
}
