﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson.Serialization;

namespace LendFoundry.SimulationServer.Persistence
{
    public class SimulationServerRepository : MongoRepository<ISimulationServerDocuments, SimulationServerDocuments>, ISimulationServerRepository
    {
        static SimulationServerRepository()
        {
            BsonClassMap.RegisterClassMap<SimulationServerDocuments>(map =>
            {
                map.AutoMap();
                var type = typeof(SimulationServerDocuments);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public SimulationServerRepository(IMongoConfiguration configuration)
            : base(new FakeTenantService(), configuration, "SimulationServerDocuments")
        {
            
        }

    }

   
}
