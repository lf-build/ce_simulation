﻿using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;

namespace LendFoundry.SimulationServer.Persistence
{
    public class FakeTenantService : ITenantService
    {
        public TenantInfo Current
        {
            get
            {
                return new TenantInfo() { Id = "my-tenant" };
            }
        }

        public List<TenantInfo> GetActiveTenants()
        {
            throw new NotImplementedException();
        }
    }
}
