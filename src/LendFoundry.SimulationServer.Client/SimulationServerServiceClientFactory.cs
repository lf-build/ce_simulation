﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.SimulationServer.Client
{
    public class SimulationServerServiceClientFactory : ISimulationServerServiceClientFactory
    {
        public SimulationServerServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }
        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public ISimulationServerService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new SimulationServerServiceClient(client);
        }
    }
}
