﻿using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.SimulationServer.Client
{
   public interface ISimulationServerServiceClientFactory
    {
        ISimulationServerService Create(ITokenReader reader);
    }
}
