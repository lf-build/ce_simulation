﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.SimulationServer.Client
{
    public static class SimulationServerServiceClientExtensions
    {
        public static IServiceCollection AddSimulationServerService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ISimulationServerServiceClientFactory>(p => new SimulationServerServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ISimulationServerServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
