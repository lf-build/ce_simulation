﻿using System.Collections.Generic;

namespace LendFoundry.SimulationServer
{
    public interface ISimulationServerService
    {
        string AddDocument(string module, AddDocument document);
        string DeleteDocument(string module, UpdateDocument document);
        object GetDocument(string module, object postedParams, Dictionary<string, string> headers, Dictionary<string, string> formparams);
        string UpdateDocument(string module, UpdateDocument document);
        object GetByGetDocument(string module, Dictionary<string, string> paramObject);

        object GetCibilDocument(string module, Envelope request);


    }
}