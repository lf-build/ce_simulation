﻿using Newtonsoft.Json;
#if DOTNET2
using System.Web;
#else
using RestSharp.Contrib;
#endif
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Net;

namespace LendFoundry.SimulationServer
{
    public class SimulationServerService : ISimulationServerService
    {
        public SimulationServerService(ISimulationServerRepository repository)
        {
            if (repository == null)
                throw new ArgumentNullException(nameof(repository));

            Repository = repository;
        }

        private ISimulationServerRepository Repository { get; }


        public string AddDocument(string module, AddDocument document)
        {
            if (string.IsNullOrWhiteSpace(module))

                throw new ArgumentNullException(nameof(module));
            if (document == null)
                throw new ArgumentNullException(nameof(document));
            if (document.Document == null)
                throw new ArgumentNullException(nameof(document.Document));

            ISimulationServerDocuments dbDocument = new SimulationServerDocuments();
            try
            {
                ISimulationServerDocuments updatedbDocument = Repository.All(i => i.ModuleName == module && i.IsXml == document.IsXml && i.MinimumMatchRequiredForSearch == document.MinimumMatchRequiredForSearch
                                           && i.SearchParameters.Equals(document.SearchParameters)).Result.FirstOrDefault();
                if (updatedbDocument != null)
                {
                    UpdateDocument updatedoc = new UpdateDocument()
                    {
                        Document = document.Document,
                        DocumentId = updatedbDocument.Id,
                        IsXml = document.IsXml,
                        MinimumMatchRequiredForSearch = document.MinimumMatchRequiredForSearch,
                        SearchParameters = document.SearchParameters


                    };
                    UpdateDocument(module, updatedoc);
                    return dbDocument.Id;
                }
            }
            catch
            {

            }
            dbDocument.ModuleName = module;
            dbDocument.SearchParameters = document.SearchParameters;
            dbDocument.IsXml = document.IsXml;
            dbDocument.Document = document.Document;
            dbDocument.MinimumMatchRequiredForSearch = document.MinimumMatchRequiredForSearch;

            Repository.Add(dbDocument);

            return dbDocument.Id;
        }

        public string UpdateDocument(string module, UpdateDocument document)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));
            if (document == null)
                throw new ArgumentNullException(nameof(document));
            if (document.Document == null)
                throw new ArgumentNullException(nameof(document.Document));

            ISimulationServerDocuments dbDocument = null;

            try
            {
                dbDocument = Repository.Get(document.DocumentId).Result;
            }
            catch
            {

            }

            if (dbDocument != null)
            {
                dbDocument.ModuleName = module;
                dbDocument.SearchParameters = document.SearchParameters;
                dbDocument.IsXml = document.IsXml;
                dbDocument.Document = document.Document;
                dbDocument.MinimumMatchRequiredForSearch = document.MinimumMatchRequiredForSearch;

                Repository.Update(dbDocument);

                return dbDocument.Id;

            }

            return "Invalid Id";
        }

        public string DeleteDocument(string module, UpdateDocument document)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));
            if (document == null)
                throw new ArgumentNullException(nameof(document)); ;

            ISimulationServerDocuments dbDocument = null;

            try
            {

                dbDocument = Repository.Get(document.DocumentId).Result;
            }
            catch
            {

            }

            if (dbDocument != null)
            {
                Repository.Remove(dbDocument);

                return dbDocument.Id;
            }

            return "Invalid Id";
        }

        public object GetCibilDocument(string module, Envelope request)
        {


            object returnValue = null;
            List<ISimulationServerDocuments> allDocuments = null;
            try
            {
                allDocuments = Repository.All(r => r.ModuleName.Equals(module)).Result.ToList();
            }
            catch (Exception exc)
            {

            }
            XDocument xDocTarget = null;
            if (allDocuments != null)
            {
                foreach (var document in allDocuments)
                {
                    if (document.IsXml)
                    {
                        if (document.SearchParameters != null && document.SearchParameters.Count > 0)
                        {
                            try
                            {
                                xDocTarget = XDocument.Parse(document.Document);
                            }
                            catch (Exception ex)
                            {
                                // throw new Exception(document.SearchParameters+""+ex.StackTrace);
                            }
                            try
                            {
                                int matchCount = 0;
                                foreach (var kvp in document.SearchParameters)
                                {
                                    XElement source = null;
                                    try
                                    {

                                        if (source == null)
                                        {

                                            //   var doc = XDocument.Parse("<?xml version=\"1.0\" encoding=\"utf-8\"?><DCRequest xmlns=\"http://transunion.com/dc/extsvc\"><Authentication type=\"OnDemand\"><UserId>CreditExchange_Admin</UserId><Password>Password@123</Password></Authentication><RequestInfo><SolutionSetId>1135</SolutionSetId><SolutionSetVersion>26</SolutionSetVersion><ExecutionMode>NewWithContext</ExecutionMode>		</RequestInfo><UserData /><Fields><Field key=\"EnvironmentType\">U</Field><Field key=\"InpEnquiryPurpose\">01</Field><Field key=\"InpScoreType\">04</Field><Field key=\"ApplicantFirstName\">Mabel</Field><Field key=\"ApplicantMiddleName\"></Field><Field key=\"ApplicantLastName\">Rajagopalan</Field><Field key=\"ExternalApplicationId\">231243214</Field><Field key=\"Gender\">Female</Field><Field key=\"DateOfBirth\">02/06/1981</Field><Field key=\"EmailID\"></Field><Field key=\"PanNo\">ADZPR4147H</Field><Field key=\"DLNumber\"></Field><Field key=\"VoterId\">UBV2099604</Field><Field key=\"AdharNumber\">898230945645</Field><Field key=\"PassportNumber\"></Field><Field key=\"RationCardNo\"></Field><Field key=\"AdditionalID1\"></Field><Field key=\"AdditionalID2\"></Field><Field key=\"Address1Line1\">501 Fifth Floor</Field><Field key=\"Address1Line2\">Pam Ville DMonte Park</Field><Field key=\"Address1Line3\"></Field><Field key=\"Address1City\">Mumbai</Field><Field key=\"Address1StateCode\">27</Field><Field key=\"Address1Pincode\">400050</Field><Field key=\"Address1Category\">01</Field><Field key=\"Address1ResCode\">01</Field><Field key=\"Address2Line1\">HSBC Bank India</Field><Field key=\"Address2Line2\">Business Park</Field><Field key=\"Address2Line3\"></Field><Field key=\"Address2City\">Mumbai</Field><Field key=\"Address2StateCode\">27</Field><Field key=\"Address2Pincode\">400013</Field><Field key=\"Address2Category\"></Field><Field key=\"Address2ResCode\"></Field><Field key=\"TelePhoneNumber1\">9012783067</Field><Field key=\"TelePhoneNumber1Type\">01</Field><Field key=\"TelePhoneNumber2\">9345678109</Field><Field key=\"TelePhoneNumber2Type\">02</Field><Field key=\"TelePhoneNumber3\"></Field><Field key=\"TelePhoneNumber3Type\">03</Field><Field key=\"TelePhoneNumber4\"></Field><Field key=\"TelePhoneNumber4Type\"></Field><Field key=\"LoanAmount\">800000</Field><Field key=\"AccountNumber1\"></Field><Field key=\"AccountNumber2\"></Field><Field key=\"AccountNumber3\"></Field><Field key=\"AccountNumber4\"></Field><Field key=\"IsCIRReq\">Y</Field><Field key=\"IsEverifyReq\">Y</Field><Field key=\"ClientID\">21</Field><Field key=\"ekyc\"></Field></Fields></DCRequest>");//request.Body.ExecuteXMLString.Request
                                            var doc = XDocument.Parse(request.Body.ExecuteXMLString.Request);//
                                            XNamespace ns = "http://transunion.com/dc/extsvc";
                                            XElement xelement = doc.Elements(ns + "DCRequest").FirstOrDefault();
                                            var x = (from phoneno in xelement.Elements(ns + "Fields")

                                                     select phoneno);
                                            XElement fielddata =
                                                x.Elements(ns + "Field").Where(t => (string)t.Attribute("key") == "Applicants")?.FirstOrDefault();

                                            var cdatanode = fielddata.FirstNode.ToString().Replace("<![CDATA[","").Replace("]]>","");
                                             var deCodeResponse = WebUtility.HtmlDecode (cdatanode);
                                            var applicants = XDocument.Parse(deCodeResponse);
                                            source = applicants.Elements("Applicants").Elements("Applicant").Elements("Telephones").Elements("Telephone").Where(i => i.Element("TelephoneType").Value == "01").Elements("TelephoneNumber")?.FirstOrDefault();
                                            //source = doc.XPathSelectElement("/*[local-name()='DCRequest']/*[local-name()='Fields']/*[local-name()='Field']");
                                            if (source != null)
                                            {
                                                var destination = string.Empty;

                                                if (kvp.Value.StartsWith("Value('"))
                                                {
                                                    destination = kvp.Value.Substring(7, kvp.Value.Length - 9);
                                                }
                                                else
                                                {
                                                    destination = xDocTarget.XPathSelectElement(kvp.Value).Value;
                                                }

                                                if (!string.IsNullOrWhiteSpace(source.Value) && !string.IsNullOrWhiteSpace(destination)
                                                    && source.Value.ToLower().Equals(destination.ToLower()))
                                                {
                                                    matchCount = matchCount + 1;
                                                }

                                                if (matchCount >= document.MinimumMatchRequiredForSearch)
                                                {
                                                    returnValue = document.Document;

                                                    return returnValue;
                                                }
                                                // break;

                                            }

                                        }
                                    }
                                    catch (Exception exc)
                                    {

                                    }


                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }

            }
            return returnValue;
        }

        public object GetDocument(string module, object postedParams, Dictionary<string, string> headers, Dictionary<string, string> formdata)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));

            object returnValue = null;

            List<ISimulationServerDocuments> allDocuments = null;

            try
            {
                allDocuments = Repository.All(r => r.ModuleName.Equals(module)).Result.ToList();
            }
            catch (Exception exc)
            {

            }

            if (allDocuments != null)
            {
                foreach (var document in allDocuments)
                {
                    if (document.IsXml)
                    {
                        if (document.SearchParameters != null && document.SearchParameters.Count > 0)
                        {
                            try
                            {
                                List<XDocument> headerDocuments = new List<XDocument>();
                                List<XDocument> formdataDocuments = new List<XDocument>();
                                foreach (var headerValue in headers.Values)
                                {
                                    XDocument tDoc = null;
                                    try
                                    {
                                        tDoc = XDocument.Parse(headerValue);
                                    }
                                    catch
                                    {

                                    }
                                    if (tDoc != null)
                                    {
                                        headerDocuments.Add(tDoc);
                                    }
                                }
                                foreach (var formValue in formdata.Values)
                                {
                                    XDocument tDoc = null;
                                    try
                                    {
                                        tDoc = XDocument.Parse(formValue);
                                    }
                                    catch
                                    {

                                    }
                                    if (tDoc != null)
                                    {
                                        formdataDocuments.Add(tDoc);
                                    }
                                }
                                XDocument xDocSource = null;
                                try
                                {
                                    xDocSource = XDocument.Parse(Convert.ToString(postedParams));
                                }
                                catch
                                {

                                }

                                XDocument xDocTarget = XDocument.Parse(document.Document);

                                int matchCount = 0;
                                foreach (var kvp in document.SearchParameters)
                                {
                                    XElement source = null;
                                    try
                                    {
                                        try
                                        {
                                            source = xDocSource.XPathSelectElement(kvp.Key);
                                        }
                                        catch
                                        {

                                        }
                                        if (source == null)
                                        {
                                            foreach (var doc in headerDocuments)
                                            {
                                                source = doc.XPathSelectElement(kvp.Key);
                                                if (source != null)
                                                {

                                                    break;

                                                }
                                            }
                                            foreach (var doc in formdataDocuments)
                                            {
                                                source = doc.XPathSelectElement(kvp.Key);
                                                if (source != null)
                                                {
                                                    if (kvp.Key != "//payload/institutionId")
                                                    {
                                                        break;

                                                    }
                                                    else
                                                    {
                                                        xDocTarget.Document.XPathSelectElement("//Success/perfiosTransactionId").Value = doc.XPathSelectElement("//payload/institutionId").Value + "_0" + (DateTime.Now.Hour / 3);
                                                        document.Document = xDocTarget.ToString();
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception exc)
                                    {

                                    }

                                    var destination = string.Empty;

                                    if (kvp.Value.StartsWith("Value('"))
                                    {
                                        destination = kvp.Value.Substring(7, kvp.Value.Length - 9);
                                    }
                                    else
                                    {
                                        destination = xDocTarget.XPathSelectElement(kvp.Value).Value;
                                    }

                                    if (!string.IsNullOrWhiteSpace(source.Value) && !string.IsNullOrWhiteSpace(destination)
                                        && source.Value.ToLower().Equals(destination.ToLower()))
                                    {
                                        matchCount = matchCount + 1;
                                    }

                                    if (matchCount >= document.MinimumMatchRequiredForSearch)
                                    {
                                        returnValue = document.Document;

                                        return returnValue;
                                    }
                                }
                            }
                            catch (Exception exc)
                            {

                            }
                        }
                        else
                        {
                            returnValue = document.Document;

                            return returnValue;
                        }
                    }
                    else
                    {
                        if (document.SearchParameters != null && document.SearchParameters.Count > 0)
                        {
                            try
                            {
                                dynamic sourceObject = JsonConvert.DeserializeObject(Convert.ToString(postedParams));
                                dynamic destinationObject = JsonConvert.DeserializeObject(Convert.ToString(document.Document));

                                int matchCount = 0;
                                foreach (var kvp in document.SearchParameters)
                                {
                                    string[] source = kvp.Key.Split(new char[] { '.' });
                                    string[] destination = null;
                                    if (!kvp.Value.StartsWith("Value('"))
                                    {
                                        destination = kvp.Value.Split(new char[] { '.' });
                                    }

                                    string sourceVal = string.Empty;
                                    if (source.Length >= 0)
                                    {
                                        dynamic runningObject = sourceObject[source[0]];
                                        for (int count = 1; count < source.Length && runningObject != null; count++)
                                        {
                                            runningObject = runningObject[source[count]];
                                        }

                                        sourceVal = Convert.ToString(runningObject);
                                    }

                                    string destinationVal = string.Empty;
                                    if (destination != null && destination.Length >= 0)
                                    {
                                        dynamic runningObject = destinationObject[destination[0]];
                                        for (int count = 1; count < source.Length && runningObject != null; count++)
                                        {
                                            runningObject = runningObject[destination[count]];
                                        }

                                        destinationVal = Convert.ToString(runningObject);
                                    }
                                    else
                                    {
                                        destinationVal = kvp.Value.Substring(7, kvp.Value.Length - 9);
                                    }

                                    if (!string.IsNullOrWhiteSpace(sourceVal) && !string.IsNullOrWhiteSpace(destinationVal) && sourceVal == destinationVal)
                                    {
                                        matchCount = matchCount + 1;
                                    }

                                    if (matchCount >= document.MinimumMatchRequiredForSearch)
                                    {
                                        returnValue = JsonConvert.DeserializeObject(document.Document);

                                        return returnValue;
                                    }
                                }
                            }
                            catch
                            {

                            }
                        }
                        else
                        {
                            returnValue = JsonConvert.DeserializeObject(document.Document);
                            return returnValue;
                        }
                    }
                }
            }

            return returnValue;
        }

        public object GetTowerdataDocument(string module, Dictionary<string, string> paramObject)
        {
            object returnValue = null;
            List<ISimulationServerDocuments> allDocuments = null;
            try
            {
                var email = HttpUtility.UrlDecode(paramObject["email"]);
                string[] emailval = email.Split(new char[] { '@' });
                if (emailval[1] == "sigmainfo.net")
                {
                    return JsonConvert.DeserializeObject("{\"email\":{\"ok\":true,\"validation_level\":5,\"status_code\":50,\"status_desc\":\"Syntax OK, domain exists, and mailbox does not reject mail\",\"address\":\"" + email + "\",\"username\":\"" + emailval[0] + "\",\"domain\":\"" + emailval[1] + "\",\"domain_type\":\"freeisp\",\"role\":false},\"status_code\":10,\"status_desc\":\"Success\"}");
                }
            }
            catch (Exception exc)
            {

            }


            try
            {
                allDocuments = Repository.All(r => r.ModuleName.Equals(module)).Result.ToList();
            }
            catch (Exception exc)
            {

            }
            if (allDocuments != null)
            {
                try
                {
                    foreach (var document in allDocuments)
                    {
                        if (document.SearchParameters != null && document.SearchParameters.Count > 0)
                        {

                            int matchCount = 0;
                            foreach (var kvp in document.SearchParameters)
                            {
                                string[] source = kvp.Key.Split(new char[] { '.' });
                                string destinationVal = string.Empty;
                                destinationVal = kvp.Value.Substring(7, kvp.Value.Length - 9);
                                string sourceVal = "";
                                sourceVal = HttpUtility.UrlDecode(paramObject[source[0]]);
                                string[] emailval = sourceVal.Split(new char[] { '@' });
                                dynamic jsonObj = JsonConvert.DeserializeObject(document.Document);
                                jsonObj["email"]["emailAddress"] = sourceVal;
                                jsonObj["email"]["username"] = emailval[0];
                                jsonObj["email"]["domain"] = emailval[1];
                                document.Document = jsonObj.ToString();
                                if (!string.IsNullOrWhiteSpace(sourceVal) && !string.IsNullOrWhiteSpace(destinationVal) && sourceVal.Contains(destinationVal))
                                {
                                    matchCount = matchCount + 1;
                                }

                                if (matchCount >= document.MinimumMatchRequiredForSearch)
                                {
                                    returnValue = JsonConvert.DeserializeObject(document.Document);

                                    return returnValue;
                                }
                            }
                        }

                        returnValue = JsonConvert.DeserializeObject(document.Document);

                    }
                }
                catch (Exception ex)
                {

                }

            }
            return returnValue;
        }
        public object GetByGetDocument(string module, Dictionary<string, string> paramObject)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));
            if (module == "towerdata")
            {
                return GetTowerdataDocument(module, paramObject);
            }
            object returnValue = null;

            List<ISimulationServerDocuments> allDocuments = null;

            try
            {
                allDocuments = Repository.All(r => r.ModuleName.Equals(module)).Result.ToList();
            }
            catch (Exception exc)
            {

            }

            if (allDocuments != null)
            {
                foreach (var document in allDocuments)
                {
                    if (document.IsXml)
                    {
                        if (document.SearchParameters != null && document.SearchParameters.Count > 0)
                        {
                            XDocument xDocTarget = XDocument.Parse(document.Document);

                            int matchCount = 0;
                            foreach (var kvp in document.SearchParameters)
                            {
                                var source = paramObject[kvp.Key];
                                var destination = xDocTarget.XPathSelectElement(kvp.Value);

                                if (!string.IsNullOrWhiteSpace(source) && !string.IsNullOrWhiteSpace(destination.Value)
                                    && source.ToLower().Equals(destination.Value.ToLower()))
                                {
                                    matchCount = matchCount + 1;
                                }

                                if (matchCount >= document.MinimumMatchRequiredForSearch)
                                {
                                    returnValue = document.Document;

                                    return returnValue;
                                }
                            }
                        }
                        else
                        {
                            returnValue = JsonConvert.DeserializeObject(document.Document);

                            return returnValue;
                        }
                    }
                    else
                    {
                        if (document.SearchParameters != null && document.SearchParameters.Count > 0)
                        {
                            dynamic sourceObject = JsonConvert.DeserializeObject(Convert.ToString(document.Document));

                            int matchCount = 0;
                            foreach (var kvp in document.SearchParameters)
                            {
                                string[] source = kvp.Key.Split(new char[] { '.' });

                                string sourceVal = string.Empty;
                                if (source.Length >= 0)
                                {
                                    dynamic runningObject = sourceObject[source[0]];
                                    for (int count = 1; count < source.Length; count++)
                                    {
                                        runningObject = runningObject[source[count]];
                                    }

                                    sourceVal = Convert.ToString(runningObject);
                                }

                                string destinationVal = paramObject[kvp.Key];

                                if (!string.IsNullOrWhiteSpace(sourceVal) && !string.IsNullOrWhiteSpace(destinationVal) && sourceVal == destinationVal)
                                {
                                    matchCount = matchCount + 1;
                                }

                                if (matchCount >= document.MinimumMatchRequiredForSearch)
                                {
                                    returnValue = JsonConvert.DeserializeObject(document.Document);

                                    return returnValue;
                                }
                            }
                        }

                        returnValue = JsonConvert.DeserializeObject(document.Document);

                        return returnValue;
                    }
                }
            }

            return returnValue;
        }


    }
}
