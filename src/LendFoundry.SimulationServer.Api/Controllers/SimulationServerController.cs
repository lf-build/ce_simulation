﻿using System;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;

#else
using Microsoft.AspNet.Mvc;
    
#endif
using System;
using System.Collections.Generic;
using System.Xml;
using  LendFoundry.Foundation.Services;


namespace LendFoundry.SimulationServer.Api.Controllers
{
    [Route("/")]
    public class SimulationServerController : Controller
    {
        public SimulationServerController(ISimulationServerService service)
        {
            if (service == null)
                throw new ArgumentNullException(nameof(service));

            Service = service;
        }

        private ISimulationServerService Service { get; }

        [HttpPut("{module}/add")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
#endif
        public IActionResult AddDocument([FromRoute]string module, [FromBody] AddDocument document)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));
            if (document == null)
                throw new ArgumentNullException(nameof(document));

            return new ObjectResult(Service.AddDocument(module, document));
        }

        [HttpPut("{module}/update")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
#endif
        public IActionResult UpdateDocument([FromRoute]string module, [FromBody] UpdateDocument document)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));
            if (document == null)
                throw new ArgumentNullException(nameof(document));

            return new ObjectResult(Service.UpdateDocument(module, document));
        }

        [HttpPut("{module}/delete")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
#endif
        public IActionResult DeleteDocument([FromRoute]string module, [FromBody] UpdateDocument document)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));
            if (document == null)
                throw new ArgumentNullException(nameof(document));

            return new ObjectResult(Service.DeleteDocument(module, document));
        }

        [HttpPost("cibil")]
#if DOTNET2
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
#endif
        public IActionResult GetDocumentCibil([FromBody] Envelope document)
        {
            return new ObjectResult(Service.GetCibilDocument("cibil", document));
        }

        [HttpPost("perfios-upload-document")]
        [HttpPost("perfios")]
        [HttpPost("perfios-comlete-transaction")]
        
        [Produces("text/plain")]
      #if DOTNET2
        [ProducesResponseType(typeof(string),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
       
#endif
        
        public IActionResult GetDocument()
        {
            
            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var header in HttpContext.Request.Headers)
            {
                headers.Add(header.Key, header.Value);
            }
           
            Dictionary<string, string> formdata = new Dictionary<string, string>();
            if (Request.Path.Value == "/perfios")
            {
                foreach (var data in HttpContext.Request.Form)
                {
                    formdata.Add(data.Key, data.Value);
                }
                return new ObjectResult(Service.GetDocument("perfios", null, headers, formdata));
            }
            else if(Request.Path.Value == "/perfios-upload-document")
            {
                return new ObjectResult(Service.GetDocument("perfios-upload-document", null, headers, formdata));
            }
            else if(Request.Path.Value == "/perfios-comlete-transaction")
            {
                return new ObjectResult(Service.GetDocument("perfios-comlete-transaction", null, headers, formdata)); 
            }

            return null;

        }
        
        [HttpPost("{module}/{a?}/{b?}/{c?}/{d?}/{e?}/{f?}")]
        
       
#if DOTNET2
        [ProducesResponseType(typeof(object),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        #else
        
#endif
        
        public IActionResult GetDocument([FromRoute]string module, [FromBody] object document)
        {
            
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));

            Dictionary<string, string> headers = new Dictionary<string, string>();
            foreach (var header in HttpContext.Request.Headers)
            {
                headers.Add(header.Key, header.Value);
            }
            Dictionary<string, string> formdata = new Dictionary<string, string>();
            if (module == "perfios")
            {
                foreach (var data in HttpContext.Request.Form)
                {
                    formdata.Add(data.Key, data.Value);
                }
                
            }
            return new ObjectResult(Service.GetDocument(module, document, headers, formdata));
        }


        [HttpGet("{module}/{a?}/{b?}/{c?}/{d?}/{e?}/{f?}")]
        [Produces("application/json", "application/xml","multipart/form-data")]
#if DOTNET2
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        #else

#endif
        public IActionResult GetDocumentGet([FromRoute]string module,
            [FromRoute] string a,
            [FromRoute] string b,
            [FromRoute] string c,
            [FromRoute] string d,
            [FromRoute] string e,
            [FromRoute] string f)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new ArgumentNullException(nameof(module));

            Dictionary<string, string> paramObject = new Dictionary<string, string>();
            paramObject.Add("a", a);
            paramObject.Add("b", b);
            paramObject.Add("c", c);
            paramObject.Add("d", d);
            paramObject.Add("e", e);
            paramObject.Add("f", f);

            if (Request.QueryString.HasValue)
            {
                string[] values = Request.QueryString.Value.Substring(1).Split(new char[] { '&' });

                foreach (var val in values)
                {
                    string[] vals = val.Split(new char[] { '=' });
                    paramObject.Add(vals[0], vals[1]);
                }
            }

            return new ObjectResult(Service.GetByGetDocument(module, paramObject));
        }

    }
}
