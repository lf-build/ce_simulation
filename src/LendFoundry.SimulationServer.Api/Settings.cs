﻿using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.SimulationServer.Api
{
    public static class Settings
    {
        public static string ServiceName { get; } = "simluationserver";

        private static string Prefix { get; } = ServiceName.ToUpper();

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

		public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "SimulationServer");
       
    }
}
