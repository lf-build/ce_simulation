﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.SimulationServer.Persistence;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc.Formatters;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using System;
using System.Threading.Tasks;

namespace LendFoundry.SimulationServer.Api
{
    public class Startup
    {

        public void ConfigureServices(IServiceCollection services)
        {
            #if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "SimulationServer"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.SimulationServer.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            //services.AddTenantTime();
            //services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            //services.AddConfigurationService<ExpressionParserConfig>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            //services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port);
            //services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);

            //services.AddConfigurationService<ExpressionParserConfig>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));

            services.AddTransient<ISimulationServerRepository, SimulationServerRepository>();
            services.AddTransient<ISimulationServerService, SimulationServerService>();

            services.AddMvc()
                .AddLendFoundryJsonOptions()
                .AddXmlSerializerFormatters()
                .AddXmlDataContractSerializerFormatters();
            services.AddMvc(option => {
                option.InputFormatters.Add(new XmlSerializerInputFormatter());
                option.OutputFormatters.Add(new XmlSerializerOutputFormatter());
            });
            services.AddCors();
        }
        //public class ErrorHandlingMiddleware
        //{
        //    public ErrorHandlingMiddleware(RequestDelegate next)
        //    {
        //        Next = next;
        //    }

        //    private RequestDelegate Next { get; }

        //    public async Task Invoke(HttpContext context)
        //    {
        //        try
        //        {
        //            await Next.Invoke(context);
        //        }
        //        catch (NotFoundException)
        //        {
        //            var response = context.Response;

        //            if (response.HasStarted)
        //                return;

        //            response.StatusCode = StatusCodes.Status404NotFound;
        //        }
        //        catch (InvalidArgumentException invalidArgument)
        //        {
        //            WriteError(StatusCodes.Status400BadRequest, invalidArgument.Message, context.Response);
        //        }
        //        catch (Exception e)
        //        {
        //            WriteError(StatusCodes.Status500InternalServerError, "We couldn't process your request", context.Response);
        //        }
        //    }

        //    private async void WriteError(int statusCode, string errorMessage, HttpResponse response)
        //    {
        //        if (response.HasStarted)
        //            return;

        //        response.StatusCode = statusCode;
        //        response.ContentType = "application/json";
        //        await response.WriteAsync(Jil.JSON.Serialize(new Error(statusCode, errorMessage)));
        //    }
        //}

       
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            #if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "SimulationServer Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            //app.UseEventHub();
            app.UseHealthCheck();
        }
    }
}
