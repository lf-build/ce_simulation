FROM microsoft/dotnet:2.0.6-sdk-2.1.101-stretch AS build-env
ARG RESTORE
RUN : "${RESTORE:?Build argument needs to be set and non-empty.}"
WORKDIR /app

COPY ./src/ /app/
COPY ./Directory.Build.props /app


WORKDIR /app/LendFoundry.SimulationServer.Api
RUN $RESTORE && dotnet publish -c Release -o out --no-restore

# Build runtime image
FROM microsoft/aspnetcore:2.0.6
WORKDIR /app/
COPY --from=build-env /app/LendFoundry.SimulationServer.Api/out .

ENTRYPOINT ["dotnet", "LendFoundry.SimulationServer.Api.dll"]